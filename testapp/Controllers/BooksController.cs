﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testapp.Model;
using testapp.Model.Interfaces;
using testapp.Model.Repositories;
using testapp.Models;

namespace testapp.Controllers
{
    public class BooksController : Controller
    {
        public IBooksRepository BooksRepository { get; set; }
        public IGenresRepository GenresRepository { get; set; }
        public IUnitOfWork UnitOfWork { get; set; }

        public BooksController(IBooksRepository booksRepository, IGenresRepository genresRepository, IUnitOfWork unitOfWork)
        {
            BooksRepository = booksRepository;
            GenresRepository = genresRepository;
            UnitOfWork = unitOfWork;
        }

        // GET: Books
        public ActionResult Index(BooksFilteredModel filteredModel)
        {
            var books = BooksRepository.FindAll();
            filteredModel.ProcessData(books);

            return View(filteredModel);
        }

        public ActionResult Create()
        {
            ViewBag.Genres = new SelectList(GenresRepository.FindAll().ToList(), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Book model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Genres = new SelectList(GenresRepository.FindAll().ToList(), "Id", "Name");
                return View(model);
            }

            BooksRepository.Insert(model);
            UnitOfWork.Commit();

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var book = BooksRepository.GetById(id);

            ViewBag.Genres = new SelectList(GenresRepository.FindAll().ToList(), "Id", "Name");
            return View(book);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Book model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.Genres = new SelectList(GenresRepository.FindAll().ToList(), "Id", "Name");
                return View(model);
            }

            var book = BooksRepository.GetById(model.Id);
            book.GenreId = model.GenreId;
            book.Author = model.Author;
            book.Title = model.Title;
            book.PublishYear = model.PublishYear;
            UnitOfWork.Commit();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var book = BooksRepository.GetById(id);
            BooksRepository.Delete(book);
            UnitOfWork.Commit();
            return RedirectToAction("Index");
        }
    }
}