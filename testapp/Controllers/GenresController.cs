﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using testapp.Model;
using testapp.Model.Interfaces;
using testapp.Model.Repositories;
using testapp.Models;

namespace testapp.Controllers
{
    public class GenresController : Controller
    {
        public IGenresRepository GenresRepository { get; set; }
        public IUnitOfWork UnitOfWork { get; set; }

        public GenresController(IGenresRepository genresRepository, IUnitOfWork unitOfWork)
        {
            GenresRepository = genresRepository;
            UnitOfWork = unitOfWork;
        }
        // GET: Genres
        public ActionResult Index(GenresFilteredModel filteredModel)
        {
            var genres = GenresRepository.FindAll();
            var g = GenresRepository.FindAll().ToList();
            filteredModel.ProcessData(genres);

            return View(filteredModel);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Genre model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            GenresRepository.Insert(model);
            UnitOfWork.Commit();

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            var book = GenresRepository.GetById(id);

            return View(book);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Genre model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var genre = GenresRepository.GetById(model.Id);
            genre.Name = model.Name;
            UnitOfWork.Commit();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            var genre = GenresRepository.GetById(id);
            GenresRepository.Delete(genre);
            UnitOfWork.Commit();
            return RedirectToAction("Index");
        }
    }
}