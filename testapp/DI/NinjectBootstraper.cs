﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject.Web.Common.WebHost;

namespace testapp.DI
{
    public class NinjectBootstraper
    {
        public static IKernel Kernel {get; private set;}
        public static void Initialize()
        {
            Kernel = new StandardKernel(new NinjectModule());
            DependencyResolver.SetResolver(new NinjectDependencyResolver(Kernel));
        }
    }
}