﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using Ninject.Web.Common;
using testapp.Model.Common;
using testapp.Model.Interfaces;
using testapp.Model.Repositories;
using testapp.Model.UnitOfWork;

namespace testapp.DI
{
    public class NinjectModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<IBooksRepository>().To<BooksRepository>().InRequestScope();
            Bind<IGenresRepository>().To<GenresRepository>().InRequestScope();
            Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();
            Bind<IDbContextFactory>().To<DbContextFactory>().InRequestScope();
        }
    }
}