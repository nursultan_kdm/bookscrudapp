﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using testapp.Model;
using X.PagedList;

namespace testapp.Models
{
    public class GenresFilteredModel
    {
        public int PageSize { get; set; } = 10;
        public int Page { get; set; } = 1;
        public IPagedList<Genre> Genres { get; set; }

        public void ProcessData(IQueryable<Genre> genres)
        {
            Genres = FilterData(genres).OrderByDescending(x => x.Id).ToPagedList(Page, PageSize);
        }

        public IQueryable<Genre> FilterData(IQueryable<Genre> genres)
        {
            return genres;
        }
    }
}