﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using testapp.Model;
using X.PagedList;

namespace testapp.Models
{
    public class BooksFilteredModel
    {
        public int PageSize { get; set; } = 10;
        public int Page { get; set; } = 1;
        public IPagedList<Book> Books { get; set; }

        public void ProcessData(IQueryable<Book> books)
        {
            Books = FilterData(books).OrderByDescending(x => x.Id).ToPagedList(Page, PageSize);
        }

        public IQueryable<Book> FilterData(IQueryable<Book> books)
        {
            return books;
        }

    }
}