﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Optimization;

namespace testapp.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
                     "~/Scripts/bootstrap/bootstrap.min.js",
                     "~/Scripts/chart/Chart.min.js",
                     "~/Scripts/jquery/jquery.min.js",
                     "~/Scripts/jquery-validation/jquery.validate.min.js",
                     "~/Scripts/jquery.cookie/jquery.cookie,js",
                     "~/Scripts/popper.js/umd/popper.min.js",
                     "~/Scripts/chart-home.js",
                     "~/Scripts/front.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap/bootstrap.min.css",
                      "~/Content/style.default.css",
                      "~/Content/orionicons.css",
                      "~/Content/custom.css"));
        }
    }
}