﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testapp.Model.Interfaces;

namespace testapp.Model.Common
{
    public abstract class RepositoryBase<T> where T : class
    {
        private BooksDbEntities dbContext;
        private IDbSet<T> dbSet;

        protected IDbContextFactory DbFactory { get; private set; }

        protected BooksDbEntities DbContext => dbContext ?? (dbContext = DbFactory.Init());

        protected RepositoryBase(IDbContextFactory dbFactory)
        {
            DbFactory = dbFactory;
            dbSet = DbContext.Set<T>();
        }

        public virtual IQueryable<T> FindAll()
        {
            return dbSet.AsQueryable();
        }

        public virtual T GetById(long id)
        {
            return dbSet.Find(id);
        }

        public virtual void Insert(T entity)
        {
            dbSet.Add(entity);
        }

        public virtual void Delete(T entity)
        {
            dbSet.Remove(entity);
        }
    }
}
