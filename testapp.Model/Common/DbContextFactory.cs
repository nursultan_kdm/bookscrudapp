﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testapp.Model.Interfaces;

namespace testapp.Model.Common
{
    public class DbContextFactory : Disposable, IDbContextFactory 
    {
        private BooksDbEntities dbContext;

        public BooksDbEntities Init()
        {
            return dbContext ?? (dbContext = new BooksDbEntities());
        }

        protected override void DisposeCore()
        {
            if (dbContext != null) dbContext.Dispose();
        }
    }
}
