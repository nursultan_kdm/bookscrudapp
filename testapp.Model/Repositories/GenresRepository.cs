﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testapp.Model;
using testapp.Model.Common;
using testapp.Model.Interfaces;

namespace testapp.Model.Repositories
{
    public class GenresRepository : RepositoryBase<Genre>, IGenresRepository
    {
        public GenresRepository(IDbContextFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface IGenresRepository : IRepository<Genre>
    {

    }
}
