﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testapp.Model.Common;
using testapp.Model.Interfaces;

namespace testapp.Model.Repositories
{
    public class BooksRepository : RepositoryBase<Book>, IBooksRepository
    {
        public BooksRepository(IDbContextFactory dbFactory) : base(dbFactory)
        {
        }
    }

    public interface IBooksRepository : IRepository<Book>
    {

    }


}
