﻿namespace testapp.Model.Interfaces
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}