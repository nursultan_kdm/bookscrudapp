﻿using System.Linq;

namespace testapp.Model.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> FindAll();
        T GetById(long id);
        void Insert(T entity);
        void Delete(T entity);
    }
}
