﻿using testapp.Model;

namespace testapp.Model.Interfaces
{
    public interface IDbContextFactory
    {
        BooksDbEntities Init();
    }
}