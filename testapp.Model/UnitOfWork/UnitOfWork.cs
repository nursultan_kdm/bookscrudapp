﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using testapp.Model.Interfaces;

namespace testapp.Model.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbContextFactory dbFactory;
        private BooksDbEntities dbContext;

        public UnitOfWork(IDbContextFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }

        public BooksDbEntities DbContext
        {
            get { return dbContext ?? (dbContext = dbFactory.Init()); }
        }
        public void Commit()
        {
            DbContext.Database.Log = Console.Write;
            DbContext.SaveChanges();

        }
    }
}
