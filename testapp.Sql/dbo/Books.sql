﻿CREATE TABLE [dbo].[Books]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Title] NVARCHAR(555) NOT NULL, 
    [Author] NVARCHAR(255) NOT NULL, 
    [PublishYear] INT NOT NULL, 
    [GenreId] INT NOT NULL, 
    CONSTRAINT [FK_Books_Genre] FOREIGN KEY ([GenreId]) REFERENCES [Genre]([Id])
)
